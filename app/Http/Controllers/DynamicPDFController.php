<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use App\Trip;
use App\Car;
use Illuminate\Support\Facades\Auth;

class DynamicPDFController extends Controller
{
    function getData($debut, $fin)
    {
      $trips = Trip::with(['car'])
      ->where('user_id', Auth::user()->id)
      ->whereBetween('date', [$debut, $fin])
      ->get();
        return $trips;
    }

    function pdf(Request $request)
    {
      $debut = $request['start'];
      $fin = $request['end'];
     $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($this->convert_customer_data_to_html($debut, $fin));
     return $pdf->stream();
    }

    function convert_customer_data_to_html($debut, $fin)
    {
      $total = 0;
     $trips = $this->getData($debut, $fin);
     $output = '
     <h3 align="center">Frais kilométriques</h3>
     <h3 align="center">Du '.$debut.' au '.$fin.'</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
    <th style="border: 1px solid; padding:12px;" width="20%">Véhicule</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Date</th>
    <th style="border: 1px solid; padding:12px;" width="15%">Adresse de départ</th>
    <th style="border: 1px solid; padding:12px;" width="15%">Adresse d arrivée</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Distance</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Indemnité</th>
   </tr>
     ';
     foreach($trips as $trip){
      $total = $total + $trip->compensation;
      $output .= '
      <tr>
       <td style="border: 1px solid; padding:12px;">'.$trip->car->model.'</td>
       <td style="border: 1px solid; padding:12px;">'.$trip->date.'</td>
       <td style="border: 1px solid; padding:12px;">'.$trip->start.'</td>
       <td style="border: 1px solid; padding:12px;">'.$trip->end.'</td>
       <td style="border: 1px solid; padding:12px;">'.$trip->distance.'</td>
       <td style="border: 1px solid; padding:12px;">'.$trip->compensation.'</td>
      </tr>';
    }
      $output .= '<tr><td></td><td></td><td></td><td></td>
      <td style="border: 1px solid; padding:12px;">Total</td>
      <td style="border: 1px solid; padding:12px;">'.$total.'</td></tr>
      ';

     $output .= '</table>';
     return $output;
    }
}
