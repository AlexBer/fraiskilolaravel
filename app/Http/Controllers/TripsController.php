<?php

namespace App\Http\Controllers;

use App\Trip;
use App\Car;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use Spatie\Browsershot\Browsershot;
class TripsController extends Controller
{
  public function app()
    {
        return view('welcome');
    }
  public function index()
  {
    $trips = Trip::with(['car' => function($query){
      return $query->withTrashed();
    }])->where('user_id', Auth::user()->id)->get();
      return response()->json($trips);
  }
  public function create()
  {
    $cars = Car::where('user_id', Auth::user()->id)->get();
      return view('addtrip', ['cars' => $cars]);
  }
  public function store(Request $request)
  {
      $trip = new trip($request->all());
      // return response()->json($request);
      $trip['user_id'] = Auth::user()->id;
      $car = Car::where('id', $trip['car_id'])->get();
        $key = 'itqniZ2dFZlgyUIHhzKwmadzVflof31F';
        $start = str_replace(' ', '+', $trip['start']);
        $end = str_replace(' ', '+', $trip['end']);
        $json = file_get_contents("http://open.mapquestapi.com/directions/v2/route?key=$key&from=$start&to=$end");
        $parse =  json_decode($json, $assoc = TRUE);
        $distance = $parse['route']["distance"];
        if($trip['roundtrip'] == 1){
        $distance = round($distance * 1.60934 * 2);
        }
        else {
            $distance = round($distance * 1.60934, 1);
        }
        if($car[0]['power'] > 3){
          $compensation = round($distance * 0.451, 2);
        }
        elseif($car[0]['power'] == 4){
          $compensation = round($distance * 0.518, 2);
        }
        elseif($car[0]['power'] == 5){
          $compensation = round($distance * 0.543, 2);
        }
        elseif($car[0]['power'] == 6){
          $compensation = round($distance * 0.568, 2);
        }
        else{
          $compensation = round($distance * 0.595, 2);
        }
      $trip['distance'] = $distance;
      $trip['compensation'] = $compensation;
      $trip ->save();
      return response()->json($trip);
  }
  public function destroy($id)
  {
    Trip::destroy($id);
    return response()->json(['message' => 'Le trajet a été supprimé']);
  }
  public function edit($id)
  {
    $trip = Trip::with(['car'])->find($id);
    return response()->json($trip);
  }
  public function update(Request $request, $id)
  {
    $trip = trip::find($id);
    $input = $request['Request'];
    $car = Car::where('id', $input['car_id'])->get();
    $trip['date'] = $input['date'];
    $trip['car_id'] = $input['car_id'];
    $trip['start'] = $input['start'];
    $trip['end'] = $input['end'];
    $trip['roundtrip'] = $input['roundtrip'];
    // Calcul de la distance par API
    $key = 'itqniZ2dFZlgyUIHhzKwmadzVflof31F';
    $start = str_replace(' ', '+', $trip['start']);
    $end = str_replace(' ', '+', $trip['end']);
    $json = file_get_contents("http://open.mapquestapi.com/directions/v2/route?key=$key&from=$start&to=$end");
    $parse =  json_decode($json, $assoc = TRUE);
    $distance = $parse['route']["distance"];
    if($input['roundtrip'] == 1){
    $distance = round($distance * 1.60934 * 2);
    }
    else {
        $distance = round($distance * 1.60934, 1);
    }
    $trip['distance'] = $distance;
    if($car[0]['power'] < 3){
      $compensation = round($distance * 0.451, 2);
    }
    elseif($car[0]['power'] == 4){
      $compensation = round($distance * 0.518, 2);
    }
    elseif($car[0]['power'] == 5){
      $compensation = round($distance * 0.543, 2);
    }
    elseif($car[0]['power'] == 6){
      $compensation = round($distance * 0.568, 2);
    }
    else{
      $compensation = round($distance * 0.595, 2);
    }
    $trip['compensation'] = $compensation;
    $trip->save();
      return response()->json($trip);
  }
  public function export(Request $request)
  {
    Browsershot::html("Test de création de PDF")->savePdf('example.pdf');
  }

}
