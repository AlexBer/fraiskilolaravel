<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $cars = Car::where('user_id', Auth::user()->id)->get();
        return response()->json($cars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addcar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $car = new car($request->all());
        $car['user_id'] = Auth::user()->id;
        $car ->save();
        return response()->json($car);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $cars
     * @return \Illuminate\Http\Response
     */
    public function show(Cars $cars)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $cars
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $car = car::find($id);
      return response()->json($car);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $cars
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $car = car::find($id);
      $input = $request['Request'];
      
      $car['model'] = $input['model'];
      $car['fuel'] = $input['fuel'];
      $car['registration'] = $input['registration'];
      $car['power'] = $input['power'];
      
      $car->save();
      return response()->json($car);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $cars
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      car::destroy($id);
      return response()->json(['message' => 'Le véhicule a été supprimé']);
    }
}
