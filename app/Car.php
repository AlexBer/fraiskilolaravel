<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Trip;

class Car extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'registration', 'model', 'power', 'fuel', 'id',
    ];
    public function trip()
    {
        // return $this->hasMany(Trips::class, 'car_id');
    }
}
