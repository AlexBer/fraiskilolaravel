<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Trips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('trips', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('start');
          $table->string('end');
          $table->string('distance');
          $table->date('date');
          $table->integer('roundtrip');
          $table->decimal('compensation');
          $table->unsignedBigInteger('user_id');
          $table->unsignedBigInteger('car_id');
          $table->timestamps();
          $table->foreign('user_id')->references('id')->on('users');
          $table->foreign('car_id')->references('id')->on('cars');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
