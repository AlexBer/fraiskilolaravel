<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// TripsController
Route::get('/gettrips', 'TripsController@index')->name('gettrips');
Route::post('/trip', 'TripsController@store')->name('addtrip');
Route::delete('/trip/{id}', 'TripsController@destroy')->name('deletetrip');
Route::get('/trip/{id}', 'TripsController@edit')->name('edittrip');
Route::put('/trip/{id}', 'TripsController@update')->name('updatetrip');

// CarsController
Route::get('/getcars', 'CarsController@index')->name('getcars');
Route::post('/car', 'CarsController@store')->name('addcar');
Route::delete('/car/{id}', 'CarsController@destroy')->name('deletecar');
Route::get('car/{id}', 'CarsController@edit')->name('editcar');
Route::put('car/{id}', 'CarsController@update')->name('updatecar');

// Redirection
Route::get('/{any}', 'TripsController@app')->where('any', '.*');