<!doctype html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
        <body>
          @extends('layouts.app')
          @section('content')
          <div>
            <div class="container">
              <div class="row">
              <a role="button" class="btn btn-primary" href="{{route('addtrip')}}">Ajouter</a>
              </div>
              <div class="row">
              <form target="_blank" action="{{route('export')}}" method="post"> {{csrf_field()}}
                <label class=" control-label" for="start">Date de début</label>
                <input id="start" name="start" type="date"  class="form-control input-md" required="">
                <label class=" control-label" for="start">Date de fin</label>
                <input id="end" name="end" type="date"  class="form-control input-md" required="">
                <button type="submit" class="btn btn-primary">Export</button>
              </form>
            </div>

            <table class="table">
  <thead>
    <tr>
      <th scope="col">Date</th>
      <th scope="col">Départ</th>
      <th scope="col">Arrivée</th>
      <th scope="col">Distance</th>
      <th scope="col">Compensation</th>
      <th scope="col">Véhicule</th>
      <th scope="col">Gestion</th>
    </tr>
  </thead>
    <tbody>
      @foreach ($trips as $trip)
      <tr>
        <td>{{$trip->date}}</td>
        <td>{{$trip->start}}</td>
        <td>{{$trip->end}}</td>
        <td>{{$trip->distance}}</td>
        <td>{{$trip->compensation}} Euros</td>
        <td>{{$trip->car->model}}</td>
        <td class="row">
          <a role="button" class="btn btn-primary" href="{{route('edittrip', ['id' => $trip->id])}}">Modifier</a>
          <form action="{{route('deletetrip', ['id' => $trip->id])}}" method="post">
            <input class="btn btn-danger" type="submit" value="Delete" />
            <input type="hidden" name="_method" value="delete" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </form>
        </td>
      </tr>
      @endforeach
              </tbody>
            </table>
            </div>
          </div>
@endsection
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>
