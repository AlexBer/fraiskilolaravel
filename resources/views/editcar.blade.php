@extends('layouts.app')

@section('content')

<form action="{{route('editcar', ['id' => $car->id])}}" method="post"> {{csrf_field()}}
  <input type="hidden" name="_method" value="put" />
  <div class="form-group">
<label class="col-md-4 control-label" for="model">Modèle du véhicule</label>
<div class="col-md-4">
<input id="model" name="model" type="text" placeholder="Modèle du véhicule" class="form-control input-md" value="{{$car->model}}" required="">

</div>
</div>

<!-- Text input-->
<div class="form-group">
<label class="col-md-4 control-label" for="registration">Plaque d'immatriculation</label>
<div class="col-md-4">
<input id="registration" name="registration" type="text" value="{{$car->registration}}"placeholder="Plaque d'immatriculation" class="form-control input-md" required="">
</div>
</div>

<!-- Select Multiple -->
<div class="form-group">
<label class="col-md-4 control-label" for="fuel" required="">Selectionner votre carburant</label>
<div class="col-md-4">
  <select id="fuel" name="fuel" class="form-control" multiple="multiple">
    <option @if($car->fuel == 'diesel')  selected @endif value="diesel">Diesel</option>
    <option @if($car->fuel == 'essence') selected @endif value="essence">Essence</option>
    <option @if($car->fuel == 'hybride') selected @endif value="hybride">Hybride</option>
    <option @if($car->fuel == 'electrique') selcted @endif value="electrique">Electrique</option>
  </select>
</div>
</div>

<!-- Text input-->
<div class="form-group">
<label class="col-md-4 control-label" for="power">Puissance fiscale</label>
<div class="col-md-4">
<input id="power" name="power" type="number" value="{{$car->power}}"placeholder="Puissance fiscale" class="form-control input-md" required="">
</div>
</div>

<!-- Button -->
<div class="form-group">
<label class="col-md-4 control-label" for="singlebutton"></label>
<div class="col-md-4">
  <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Modifier</button>
</div>
</div>
</form>
@endsection()
