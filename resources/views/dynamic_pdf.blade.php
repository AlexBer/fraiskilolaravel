<!DOCTYPE html>
<html>
 <head>
  <title>Prévisualisation du PDF</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
   }
  </style>
 </head>
 <body>
  <br />
  <div class="container">
   <h3 align="center">Votre indemnité kilométrique</h3><br/>
   <div class="row">
    <div class="col-md-7" align="right">
     <h4>Indemnité frais kilométriques</h4>
    </div>
    <div class="col-md-5" align="right">
     <a href="{{ url('dynamic_pdf/pdf') }}" class="btn btn-danger">Export PDF</a>
    </div>
   </div>
   <br />
   <div class="table-responsive">
    <table class="table table-striped table-bordered">
     <thead>
      <tr>
       <th>Véhicule</th>
       <th>Date</th>
       <th>Adresse de départ</th>
       <th>Adresse d'arrivée</th>
       <th>Distance</th>
       <th>Indemnité</th>
      </tr>
     </thead>
     <tbody>
     @foreach($trips as $trip)
      <tr>
        <td>{{$trip->car->model}}</td>
        <td>{{$trip->date}}</td>
        <td>{{$trip->start}}</td>
        <td>{{$trip->end}}</td>
        <td>{{$trip->distance}}</td>
        <td>{{$trip->compensation}}</td>
      </tr>
     @endforeach
     <p></p>
     </tbody>
    </table>
   </div>
  </div>
 </body>
</html>
