<!doctype html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
        <body>
          @extends('layouts.app')

@section('content')

          <div>
            <div class="container">
              <a role="button" class="btn btn-primary"href="{{route('addcar')}}">Ajouter</a>
            <table class="table">
  <thead>
    <tr>
      <th scope="col">Modèle</th>
      <th scope="col">Carburant</th>
      <th scope="col">Puissance</th>
      <th scope="col">Immatriculation</th>
      <th scope="col">Gestion</th>
    </tr>
  </thead>
    <tbody>
      @foreach ($cars as $car)
      <tr>
        <td>{{$car->model}}</td>
        <td>{{$car->fuel}}</td>
        <td>{{$car->power}}</td>
        <td>{{$car->registration}}</td>
        <td class="row">
          <a role="button" class="btn btn-primary" href="{{route('editcar', ['id' => $car->id])}}">Modifier</a>
          <form action="{{route('deletecar', ['id' => $car->id])}}" method="post">
            <input class="btn btn-danger" type="submit" value="Delete" />
            <input type="hidden" name="_method" value="delete" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </form>
        </td>
      </tr>
      @endforeach
              </tbody>
            </table>
            </div>
          </div>
@endsection
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>
