@extends('layouts.app')

@section('content')

<form class="form-horizontal" method="POST" action="{{route('addtrip')}}"> {{csrf_field()}}
<div class="form-group">
  <label class="col-md-4 control-label" for="date">Date</label>
  <div class="col-md-4">
  <input id="date" name="date" type="date"  class="form-control input-md" required="">
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="car_id">Véhicule</label>
  <div class="col-md-4">
    <select id="car_id" name="car_id" class="form-control" multiple="multiple">
      @foreach ($cars as $car)
      <option value="{{$car->id}}">{{$car->model}}</option>
      @endforeach
    </select>
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="start">Départ</label>
  <div class="col-md-4">
  <input id="start" name="start" type="text" placeholder="Lieu de départ" class="form-control input-md" required="">
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="end">Arrivée</label>
  <div class="col-md-4">
    <input id="end" name="end" type="text" placeholder="Lieu d'arrivée" class="form-control input-md" required="">
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="roundtrip">Aller/retour</label>
  <div class="col-md-4">
    <label class="radio-inline" for="roundtrip-0">
      <input type="radio" name="roundtrip" id="roundtrip-0" value="1" checked="checked">
      Oui
    </label>
    <label class="radio-inline" for="roundtrip-1">
      <input type="radio" name="roundtrip" id="roundtrip-1" value="0">
      Non
    </label>
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="sendForm" type="submit" name="sendForm" class="btn btn-primary">Ajouter</button>
  </div>
</div>
</form>
@endsection()
