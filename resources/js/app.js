/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue'
import VueRouter from 'vue-router';

import App from './components/App'
import Cars from './components/Cars'
import Trips from './components/Trips'
import AddTrip from './components/AddTrip'
import AddCar from './components/AddCar'
import EditCar from './components/EditCar'
import EditTrip from './components/EditTrip'
Vue.use(VueRouter);

window.Vue = require('vue');
import Toasted from 'vue-toasted';
 
Vue.use(Toasted)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('App', require('./components/App.vue').default);
Vue.component('Trips', require('./components/Trips.vue').default);
Vue.component('Cars', require('./components/Cars.vue').default);
Vue.component('AddTrip', require('./components/AddTrip.vue').default);
Vue.component('AddCar', require('./components/AddCar.vue').default);
Vue.component('EditCar', require('./components/EditCar.vue').default);
Vue.component('EditTrip', require('./components/EditTrip.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
        {
            path: '/trips',
            name: 'trips',
            component: Trips,
        },
        {
            path: '/addtrip',
            name: 'addtrip',
            component: AddTrip,
        },
        {
            path: '/cars',
            name: 'cars',
            component: Cars,
        },
        {
            path: '/addcar',
            name: 'addcar',
            component: AddCar,
        },
        {
            path: 'editcar:id',
            name: 'editcar',
            component: EditCar,
        },
        {
            path: 'edittrip:id',
            name: 'edittrip',
            component: EditTrip
        }
    ],
});
Vue.use(Toasted);
Vue.toasted.register('error', message => message,  {
    theme: "outline", 
    position : 'bottom-center',
    duration : 2500,
    type: 'error'
}),
Vue.toasted.register('success', message => message,  {
    theme: "outline", 
    position : 'bottom-center',
    duration : 2500,
    type: 'success'
})

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
